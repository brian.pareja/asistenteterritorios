import React from 'react';
import { StyleSheet, Platform, Image, View, ScrollView, ImageBackground } from 'react-native';
import { Tile, ButtonGroup,Text, Button,colors } from 'react-native-elements';

export default class App extends React.Component {
  render() {
    const buttons = ['Tengo un Codigo', 'Iniciar Sesión']
    return (
    <View style={styles.container}>
        <View style={styles.background}>
            <ImageBackground source={require('./../assets/images/1.png')} style={{width: '100%', height: '100%'}}>
            <Text h1 style={styles.text} >Bienvenido</Text>
            <Text h1 style={styles.text} ></Text>
            <Text h4 style={styles.text}>Selecciona una de las opciones para continuar</Text>
            <View style={styles.buttons}>
            <Button
              title="Tengo un Codigo"
              buttonStyle={{
                backgroundColor: '#364D69',
                borderRadius: 30,
                borderWidth: 1,
                borderColor: 'white',
              }}
              containerStyle={{ marginVertical: 10, height: 40, width: 320 }}
              titleStyle={{ fontWeight: 'bold' }}
            />
            <Button
              title="Iniciar Sesion"
              buttonStyle={{
                backgroundColor: '#364D69',
                borderRadius: 30,
                borderWidth: 1,
                borderColor: 'white',
              }}
              containerStyle={{ marginVertical: 10, height: 50, width: 320 }}
              titleStyle={{ fontWeight: 'bold' }}
            />
            </View>
            </ImageBackground>
        </View>
          {/*<Image source={require('./../assets/images/logo.jpg')} style={styles.image} alignContent='stretch'/>*/}
          {/*<ButtonGroup rounded
          buttons={buttons}
          containerStyle={{height: 50}}
          buttonStyle={styles.buttons}
          />*/}
        </View>
    );
  }
}
const styles = StyleSheet.create({

  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image:{
     alignItems: 'center',
     resizeMode: 'cover'
  },
  viewButton: {
      alignContent: 'stretch'
  },
  background:{
    justifyContent: 'center',
    flex:5,
    alignItems: 'center',
    textAlign: 'center',
  },
  buttons:{
    alignItems: 'center'
  },
  container:{
    flex:1
  },
  text:{
    fontFamily: 'tahoma',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    color: '#F5FCFF',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    margin: 10
  }
});