import React from 'react';
import { StyleSheet, Platform, Image, View, ScrollView, ImageBackground } from 'react-native';
import { Tile, ButtonGroup,Text, Button,colors,Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Login extends React.Component {
  render() {
    const buttons = ['Tengo un Codigo', 'Iniciar Sesión']
    return (
    <View style={styles.container}>
        <View style={styles.background}>
            <ImageBackground source={require('./../assets/images/1.png')} style={styles.background} blurRadius={8}>
            <Text h1 style={styles.text} >Iniciar Sesion</Text>
            <Input
              placeholder='Usuario'
              placeholderTextColor="white"
              containerStyle={{ marginVertical: 10 }}
              inputStyle={{ marginLeft: 10, color: 'white' }}
              leftIcon={
                <Icon
                  name='user'
                  size={24}
                  color='white'
                />
              }
            />
            <Input
              placeholder='Contraseña'
              placeholderTextColor="white"
              containerStyle={{ marginVertical: 10 }}
              inputStyle={{ marginLeft: 10, color: 'white' }}
              keyboardType="default"
              secureTextEntry={true}
              leftIcon={
                <Icon
                  name='key'
                  size={24}
                  color='white'
                />
              }
            />
            <View style={styles.buttons}>
            <Button
              title="Iniciar Sesion"
              buttonStyle={{
                backgroundColor: '#364D69',
                borderRadius: 30,
                borderWidth: 1,
                borderColor: 'white',
              }}
              containerStyle={{ marginVertical: 10, height: 50, width: 320 }}
              titleStyle={{ fontWeight: 'bold' }}
            />
            </View>
            </ImageBackground>
      </View>
    </View>
    );
  }
}
const styles = StyleSheet.create({

  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  image:{
     alignItems: 'center',
     resizeMode: 'cover'
  },
  viewButton: {
      alignContent: 'stretch'
  },
  background:{
    justifyContent: 'center',
    flex:5,
    alignItems: 'center',
    textAlign: 'center',
    width: '100%', height: '100%'
  },
  buttons:{
    marginTop:50,
    alignItems: 'center'
  },
  container:{
    flex:1
  },
  text:{
    fontFamily: 'AwesomeFont',
    alignItems: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    color: '#F5FCFF',
    justifyContent: 'space-between',
    margin: 10
  }
});