/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import Inicio from './src/app/containers/start';
import Qrscan from './src/app/containers/qrcode';
import Login from './src/app/containers/login';
import Code from './src/app/containers/code';
import List from './src/app/containers/list';
import {name as appName} from './app.json';


AppRegistry.registerComponent(appName, () => List);
